﻿const path = require("path");
const webpack = require("webpack");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
process.traceDeprecation = true;

module.exports = {
	entry: './src/application.js',
	output: {
		path: __dirname + "/dist",
		filename: "bundle.js",
	},

    module: {
        rules: [{
            test: /\.scss$/,
            use: [
                {loader: MiniCssExtractPlugin.loader},
                {loader: 'css-loader'},
                {loader: 'sass-loader'}
            ]
        },
        {
            test: /\.(png|jpe?g|gif)$/i,
            use: [
              {
                loader: 'file-loader',
                options: {
                    name: '[path][name].[ext]',
                },
                
              },
              
            ],
          },
          {
            test: /\.m?js$/,
            exclude: /(node_modules)/,
            use: {
              loader: 'babel-loader',
              options: {
                presets: ['@babel/preset-env']
              }
            }
          } 
        ]
    },

    plugins: [
        new HtmlWebpackPlugin({
            template:'./src/index.html',
        }),

        new MiniCssExtractPlugin({
            filename: 'main.css'
        })
    ]
};