import './styles/main.scss';

document.addEventListener("DOMContentLoaded", function (event) {

    fetch('https://api.jsonbin.io/b/6035350af1be644b0a63c74a')
        .then(data => data.text())
        .then(dataText => {
            let datos = JSON.parse(dataText);
            const container = document.querySelector("#cards-wrapper");
            datos.forEach((el, i) => {
                container.innerHTML += `<div class="card">
            <div class="title">
                <h3 class="t-h3">${el.nombre}</h3>
            </div>
            <div class="wrapper-includes">
                <ul>
                    ${el.includes.map(packI => {
                    return '<li><h4 class="t-h4">' + packI + '</h4></li>'
                }).join("")}
                </ul>
            </div>
            <div class="wrapper-picker">
                <label class="container">
                    <input id="checker-${i + 1}" type="radio" name="pack-picker">
                    <span class="label-text t-text">Elegir un régimen</span>
                    <span class="checkmark"></span>
                </label>
            </div>
        </div>`
            });

        }).then(() => {

            const getInput = document.querySelectorAll('input[name=pack-picker]');

            getInput.forEach((el, i) => {
                el.addEventListener('click', (e) => {
                    switch (e.target.id) {
                        case "checker-1":
                            e.target.closest('.wrapper-picker').classList.add("active")
                            document.querySelector("#checker-2").closest(".wrapper-picker").classList.remove('active');
                            document.querySelector("#checker-3").closest(".wrapper-picker").classList.remove('active');
                            document.querySelector("#checker-4").closest(".wrapper-picker").classList.remove('active');
                            break;

                        case "checker-2":
                            e.target.closest('.wrapper-picker').classList.add("active")
                            document.querySelector("#checker-1").closest(".wrapper-picker").classList.remove('active');
                            document.querySelector("#checker-4").closest(".wrapper-picker").classList.remove('active');
                            document.querySelector("#checker-3").closest(".wrapper-picker").classList.remove('active');
                            break;

                        case "checker-3":
                            e.target.closest('.wrapper-picker').classList.add("active")
                            document.querySelector("#checker-1").closest(".wrapper-picker").classList.remove('active');
                            document.querySelector("#checker-2").closest(".wrapper-picker").classList.remove('active');
                            document.querySelector("#checker-4").closest(".wrapper-picker").classList.remove('active');
                            break;

                        default:
                            e.target.closest('.wrapper-picker').classList.add("active")
                            document.querySelector("#checker-1").closest(".wrapper-picker").classList.remove('active');
                            document.querySelector("#checker-2").closest(".wrapper-picker").classList.remove('active');
                            document.querySelector("#checker-3").closest(".wrapper-picker").classList.remove('active');
                            break;
                    }
                })

            });
        });

    const xhttp = new XMLHttpRequest();
    xhttp.open('GET', 'https://api.jsonbin.io/b/603538bfa94a574b452141ab/1', true);
    xhttp.send();
    xhttp.onreadystatechange = function () {
        //carga de los datos de acordeón.
        if (this.readyState == 4 && this.status == 200) {
            let accordions = JSON.parse(this.response);
            const accordionContainer = document.querySelector('#tab2');

            accordions.forEach((el, index) => {
                accordionContainer.innerHTML += `<div class="accordion"><div class="wrapper-title">
                <h1 class="t-h1" data-target="#innerCard${index + 1}">
                    <svg>
                        <use data-target="#innerCard1" xlink:href="#arrow" />
                    </svg>${el.titleCard}
                    <small class="t-small">Opcional</small>
                </h1>
                <div class="wrapper-tag">
                    <span class="t-tag">Exclusivo hoteles.com</span>
                </div>
            </div>
            ${index == 0 ? `<div id="innerCard${index + 1}">` : `<div id="innerCard${index + 1}" class="d-none">`}
                ${el.cardInner.map((card, i) => {
                    return `<div class="inner-card">
                    <div class="wrapper-visible">
                        <div class="container-img">
                            <img src="${card.img}" alt="${i == 0 ? `Mujeres haciendo Yoga` : `Piscina SPA`}" />
                        </div>
                        <div class="wrapper-text">
                            <div class="text">
                                <h3 class="t-h3">${card.title}</h3>
                                <p class="t-text">${card.text}</p>
                            </div>
                            ${i == 0 ? `<div class="toggle">` : `<div class="toggle open">`}
                                <a class="t-link moreInfo" data-link="info${i}">Más información y condiciones</a>
                                <svg>
                                    <use xlink:href="#arrow" />
                                </svg>
                            </div>
                        </div>
                    </div>
                    ${i == 0 ? `<div id="info${i}" class="wrapper-includes d-none">` : `<div id="info${i}" class="wrapper-includes">`}
                    ${card.features.length != 0 ? `<p class="t-text">Incluye</p>` : ""}
                        <ul>
                            ${card.features.map(li => {
                                return `<li class="t-text">${li}</li>`
                            }).join("")}
                        </ul>
                        <p class="t-bold">Valores no acumulables a otras promociones similares</p>
                    </div>
                </div>`
                }).join("")}
                </div>
            </div>`
            });
        }
        ajaxDone();
    }
});

//navegación de las tabs
let getLink = document.querySelectorAll(".item-a");

for (let link of getLink) {
    link.addEventListener('click', e => {
        e.preventDefault();
        let hrefAttr = e.target.getAttribute('href');
        if (hrefAttr === "#tab1") {
            link.classList.add("active");
            document.querySelector('#link2').classList.remove("active")
            document.querySelector("#tab2").classList.add("d-none");
            document.querySelector("#tab1").classList.remove("d-none");
        } else {
            link.classList.add("active");
            document.querySelector('#link1').classList.remove("active")
            document.querySelector("#tab1").classList.add("d-none");
            document.querySelector("#tab2").classList.remove("d-none");
        }
    });
}

const ajaxDone = () => {

    //sacar el panel de servicios incluidos
    let moreInfo = document.querySelectorAll('.moreInfo');
    for (let info of moreInfo) {
        info.addEventListener('click', event => {
            let getDataLink = event.target.getAttribute('data-link');
            if (document.querySelector('#' + getDataLink).classList.contains('d-none')) {
                document.querySelector('#' + getDataLink).classList.remove('d-none');
                info.parentElement.classList.add('open');
            } else {
                document.querySelector('#' + getDataLink).classList.add('d-none');
                info.parentElement.classList.remove('open');
            }
        });
    };

    //plegar y desplegar acordeones
    let h1Title = document.querySelectorAll(".wrapper-title h1");
    for (let title of h1Title) {
        title.addEventListener('click', e => {
            let getTarget = e.target.getAttribute('data-target');
            if (document.querySelector(getTarget).classList.contains('d-none')) {
                document.querySelector(getTarget).classList.remove('d-none');
                document.querySelector(getTarget).classList.add('fade-in');
                e.target.classList.remove("close");
            } else {
                document.querySelector(getTarget).classList.add('d-none');
                document.querySelector(getTarget).classList.remove('fade-in');
                e.target.classList.add("close");
            }
        })
    }
}



